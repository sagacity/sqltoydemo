package com.zking.controller;

import com.zking.entity.User;
import com.zking.service.UserService;
import com.zking.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService=new UserServiceImpl();

    /**
     * 登录
     * @param name  名字
     * @param password  密码
     * @return
     */
    @GetMapping("/login")
    public Object userLogin(@RequestParam String name,@RequestParam String password){
        return userService.login(name,password);
    }








}
