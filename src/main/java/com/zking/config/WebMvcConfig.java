package com.zking.config;

import org.activiti.spring.SpringProcessEngineConfiguration;
import org.activiti.spring.boot.ProcessEngineConfigurationConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig  implements ProcessEngineConfigurationConfigurer {


    @Bean
    public WebMvcConfigurer corsConfigurer(){

    return new WebMvcConfigurer(){
        @Override
        public void addCorsMappings(CorsRegistry registry) {
            registry.addMapping("/**").
                    allowedMethods("GET","POST","PUT","DELETE","OPTIONS","HEAD","PATCH").
                    allowCredentials(true).
                    allowedHeaders("*").
                    allowedOrigins("*");

        }
    };
    }


    @Override
    public void configure(SpringProcessEngineConfiguration processEngineConfiguration) {
        processEngineConfiguration.setActivityFontName("宋体");
        processEngineConfiguration.setAnnotationFontName("宋体");
        processEngineConfiguration.setLabelFontName("宋体");
    }
}
