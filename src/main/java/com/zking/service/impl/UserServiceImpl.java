package com.zking.service.impl;

import com.zking.entity.User;
import com.zking.service.UserService;
import org.sagacity.sqltoy.dao.SqlToyLazyDao;
import org.sagacity.sqltoy.dao.impl.SqlToyLazyDaoImpl;
import org.sagacity.sqltoy.link.Delete;
import org.sagacity.sqltoy.link.Update;
import org.sagacity.sqltoy.model.PaginationModel;
import org.sagacity.sqltoy.support.BaseDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl extends BaseDaoSupport implements UserService {


    @Autowired
    private SqlToyLazyDao sqlToyLazyDao=new SqlToyLazyDaoImpl();


    @Override
    public Object login(String name,String password) {
        List<User> login = sqlToyLazyDao.findBySql("login", new String[]{"name", "password"}, new Object[]{name, password}, User.class);
        return login;
    }
















}
