package com.zking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.activiti.spring.boot.SecurityAutoConfiguration;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication(exclude = {
        SecurityAutoConfiguration.class})
@ImportResource("classpath:spring/spring-context.xml")
public class SqltoydemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SqltoydemoApplication.class, args);
    }

}
