package com.zking.entity.base;

import org.sagacity.sqltoy.config.annotation.Column;
import org.sagacity.sqltoy.config.annotation.Entity;
import org.sagacity.sqltoy.config.annotation.Id;

import java.io.Serializable;
import java.sql.Types;

@Entity(tableName = "user", pk_constraint = "PRIMARY")
public abstract class AbstractUser implements Serializable, java.lang.Cloneable {

    @Id(strategy = "generator", generator = "org.sagacity.sqltoy.plugin.id.UUIDGenerator")
    @Column(name = "ID", length = 32L, type = java.sql.Types.VARCHAR, nullable = false)
    protected String id;
    @Column(name = "NAME", length = 32L, type = Types.VARCHAR, nullable = false)
    protected String name;

    @Column(name = "age", length = 11L, type = Types.INTEGER, nullable = false)
    protected Integer age;

    @Column(name = "groupId",length = 32L, type = Types.VARCHAR, nullable = false)
    protected  String groupId;

    @Column(name = "password", length = 32L, type = Types.VARCHAR, nullable = false)
    protected String password;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
