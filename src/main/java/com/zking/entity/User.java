package com.zking.entity;

import com.zking.entity.base.AbstractUser;
import org.sagacity.sqltoy.config.annotation.SqlToyEntity;
import org.sagacity.sqltoy.model.PaginationModel;

@SqlToyEntity
public class User extends AbstractUser {


    private String groupName;


    public User() {

    }

    public User(String id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
