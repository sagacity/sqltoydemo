package com.zking.entity;

import java.util.Map;

public class HistoricTaskInstance {


    private Map<String, Object> variables;

    public Map<String, Object> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Object> variables) {
        this.variables = variables;
    }
}
