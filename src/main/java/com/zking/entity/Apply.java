package com.zking.entity;

import java.io.Serializable;
import java.util.Date;

public class Apply implements Serializable {

    private String personal;

    private String day;

    private String reason;

    private String updateDate;

    private String createDate;

    private String state;

    private String taskId;

    private String processDefinitionId;

    private String claimUser;

    private String claimDate;

    private String completeUser;

    private String completeDate;

    private String recordClaimUser;

    private String recordClaimDate;

    private String recordUser;

    private String recordDate;

    private Boolean endNode;

    public String getPersonal() {
        return personal;
    }

    public void setPersonal(String personal) {
        this.personal = personal;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getClaimUser() {
        return claimUser;
    }

    public void setClaimUser(String claimUser) {
        this.claimUser = claimUser;
    }

    public String getCompleteUser() {
        return completeUser;
    }

    public void setCompleteUser(String completeUser) {
        this.completeUser = completeUser;
    }

    public String getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(String completeDate) {
        this.completeDate = completeDate;
    }

    public String getRecordUser() {
        return recordUser;
    }

    public void setRecordUser(String recordUser) {
        this.recordUser = recordUser;
    }

    public String getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

    public Boolean getEndNode() {
        return endNode;
    }

    public void setEndNode(Boolean endNode) {
        this.endNode = endNode;
    }

    public String getClaimDate() {
        return claimDate;
    }

    public void setClaimDate(String claimDate) {
        this.claimDate = claimDate;
    }

    public String getRecordClaimUser() {
        return recordClaimUser;
    }

    public void setRecordClaimUser(String recordClaimUser) {
        this.recordClaimUser = recordClaimUser;
    }

    public String getRecordClaimDate() {
        return recordClaimDate;
    }

    public void setRecordClaimDate(String recordClaimDate) {
        this.recordClaimDate = recordClaimDate;
    }
}
